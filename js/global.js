//JQUERY SELECTORS
var derButton, factsButton, labButton, links,
	menuFrame, frames, model, modelContent;

//SOUNDS
var buttonSound,
	audio = [];

//OTHER GLOBAL DATA
var shots, currentShot;

//TIMEOUTS
var timeout = [];

var launch101 = function()
{
	var frame = $("#frame-101");
		startButton = $("#frame-101 #start-button"),
		nextButton = $("#frame-101 #next-button"), 
		blackSkin = $("#frame-101 .black-skin"),
		frame1Eyes = $("#frame-101 #frame-1-eyes"),
		frame2Eyes = $("#frame-101 #frame-2-eyes"),
		frame3Eyes = $("#frame-101 #frame-3-eyes"),
		frame4Eyes = $("#frame-101 #frame-4-eyes"),
		girlSpeakAnimated = $("#girl-speak-animated-1"),
		girlSpeakAnimated2 = $("#girl-speak-animated-2"),
		boySpeakAnimated = $("#boy-speak-animated-1"),
		boySpeakAnimated2 = $("#boy-speak-animated-2"),
		boySpeakAnimated3 = $("#boy-speak-animated-3"),
		boySpeakAnimated4 = $("#boy-speak-animated-4"),
		boySpeakAnimated4Eyes = $("#boy-speak-animated-4-eyes"),
		mainCloud = $("#main-cloud"),
		wideCloud = $("#wide-cloud"),
		examplePic = $("#example-pic");
	
	frameSprite = new Motio(frame[0], {
		frames: 9
	});
	
	frame1Eyes.fadeOut(0);
	frame2Eyes.fadeOut(0);
	frame3Eyes.fadeOut(0);
	frame4Eyes.fadeOut(0);
	
	nextButton.fadeOut(0);
	
	girlSpeakAnimated.fadeOut(0);
	boySpeakAnimated.fadeOut(0);
	
	boySpeakAnimated2.fadeOut(0);
	girlSpeakAnimated2.fadeOut(0);
	
	boySpeakAnimated3.fadeOut(0);
	
	boySpeakAnimated4.fadeOut(0);
	boySpeakAnimated4Eyes.fadeOut(0);
	
	mainCloud.fadeOut(0);
	wideCloud.fadeOut(0);
	
	audio[0] = new Audio("audio/girl-1.mp3"),
	audio[1] = new Audio("audio/boy-1.mp3");
	audio[2] = new Audio("audio/boy-2.mp3");
	audio[3] = new Audio("audio/boy-3.mp3");
	audio[4] = new Audio("audio/boy-4.mp3");
	audio[5] = new Audio("audio/girl-2.mp3");
	audio[6] = new Audio("audio/girl-3.mp3");
	audio[7] = new Audio("audio/girl-4.mp3");
	audio[8] = new Audio("audio/girl-5.mp3");
	audio[9] = new Audio("audio/girl-6.mp3");
	audio[10] = new Audio("audio/girl-7.mp3");
	audio[11] = new Audio("audio/girl-8.mp3");
	audio[12] = new Audio("audio/girl-9.mp3");
	audio[13] = new Audio("audio/girl-10.mp3");
	audio[14] = new Audio("audio/girl-11.mp3");
	audio[15] = new Audio("audio/girl-12.mp3");
	audio[16] = new Audio("audio/girl-14.mp3");
	audio[17] = new Audio("audio/girl-15.mp3");
	audio[18] = new Audio("audio/girl-16.mp3");
	audio[19] = new Audio("audio/girl-17.mp3");
	audio[20] = new Audio("audio/girl-18.mp3");
	audio[21] = new Audio("audio/girl-19.mp3");
	audio[22] = new Audio("audio/girl-20.mp3");
	audio[23] = new Audio("audio/girl-21.mp3");
	audio[24] = new Audio("audio/girl-22.mp3");
	audio[25] = new Audio("audio/girl-23.mp3");
	audio[26] = new Audio("audio/girl-24.mp3");
	audio[27] = new Audio("audio/boy-5.mp3");
	audio[28] = new Audio("audio/girl-25.mp3");
	audio[29] = new Audio("audio/girl-26.mp3");
	audio[30] = new Audio("audio/girl-27.mp3");
	audio[31] = new Audio("audio/girl-28.mp3");
	audio[32] = new Audio("audio/girl-29.mp3");
	audio[33] = new Audio("audio/boy-6.mp3");
	
	audio[0].addEventListener("ended", function(){
		girlSpeakAnimated.fadeOut(0);
		frame1Eyes.fadeOut(0);
		timeout[1] = setTimeout(function(){
			frameSprite.to(1, true);
			timeout[2] = setTimeout(function(){
				audio[1].play();
				boySpeakAnimated.fadeIn(0);
				frame2Eyes.fadeIn(0);
			}, 2000);			
		}, 2000);
	});
	
	audio[1].addEventListener("ended", function(){
		audio[2].play();
	});
	
	audio[2].addEventListener("ended", function(){
		audio[3].play();
	});
	
	audio[3].addEventListener("ended", function(){
		boySpeakAnimated.fadeOut(0);
		frame2Eyes.fadeOut(0);
		frameSprite.to(2, true);
		timeout[3] = setTimeout(function(){
			boySpeakAnimated2.fadeIn(0);
			frame3Eyes.fadeIn(0);
			audio[4].play();
		}, 2000);
	});
	
	audio[4].addEventListener("ended", function(){
		boySpeakAnimated2.fadeOut(0);
		frame3Eyes.fadeOut(0);
		frameSprite.to(3, true);
		timeout[4] = setTimeout(function(){
			audio[5].play();
			girlSpeakAnimated2.fadeIn(0);
			frame4Eyes.fadeIn(0);
			mainCloud.fadeIn(500);
			timeout[5] = setTimeout(function(){
				examplePic.css("background-image", "url('pics/vinegar.png')");
			}, 1000)
		}, 2000);
	});
	
	audio[5].addEventListener("ended", function(){
		audio[6].play();
	});
	
	audio[6].addEventListener("ended", function(){
		audio[7].play();
		examplePic.css("background-image", "url('pics/coffee.png')");
	});
	
	audio[7].addEventListener("ended", function(){
		audio[8].play();
		examplePic.css("background-image", "url('pics/cola-cup.png')");
	});
	
	audio[8].addEventListener("ended", function(){
		audio[9].play();
		examplePic.css("background-image", "url('pics/lemon.png')");
	});
	
	audio[9].addEventListener("ended", function(){
		audio[10].play();
		examplePic.css("background-image", "url('pics/orange.png')");
	});
	
	audio[10].addEventListener("ended", function(){
		audio[11].play();
		examplePic.css("background-image", "");
		mainCloud.fadeOut(0);
	});
	
	audio[11].addEventListener("ended", function(){
		audio[12].play();
	});
	
	audio[12].addEventListener("ended", function(){
		audio[13].play();
	});
	
	audio[13].addEventListener("ended", function(){
		audio[14].play();
		mainCloud.fadeIn(500);
		timeout[6] = setTimeout(function(){
			examplePic.css("background-image", "url('pics/acid-solution.gif')");
		}, 1000);
	});
	
	audio[14].addEventListener("ended", function(){
		audio[15].play();
	});
	
	audio[15].addEventListener("ended", function(){
		audio[16].play();
		timeout[7] = setTimeout(function(){
			examplePic.css("background-image", "url('pics/cross.png')");
		}, 2000);
		timeout[8] = setTimeout(function(){
			examplePic.css("background-image", "url('pics/caustic.png')");
		}, 4000);
	});
	
	audio[16].addEventListener("ended", function(){
		audio[17].play();
	});
	
	audio[17].addEventListener("ended", function(){
		audio[18].play();
		examplePic.css("background-image", "");
	});
	
	audio[18].addEventListener("ended", function(){
		audio[19].play();
		examplePic.css("background-image", "url('pics/hand-wash.gif')");
	});
	
	audio[19].addEventListener("ended", function(){
		audio[20].play();
		examplePic.css("background-image", "");
		mainCloud.fadeOut(500);
	});
	
	audio[20].addEventListener("ended", function(){
		audio[21].play();
	});
	
	audio[21].addEventListener("ended", function(){
		audio[22].play();
	});
	
	audio[22].addEventListener("ended", function(){
		audio[23].play();
		mainCloud.fadeIn(500);
		timeout[6] = setTimeout(function(){
			examplePic.css("background-image", "url('pics/acid-plus-metal.gif')");
		}, 1000);
	});
	
	audio[23].addEventListener("ended", function(){
		audio[24].play();
		timeout[7] = setTimeout(function(){
			examplePic.css("background-image", "url('pics/acid-plus-magnesium.gif')");
		}, 3000);
	});
	
	audio[24].addEventListener("ended", function(){
		audio[25].play();
	});
	
	audio[25].addEventListener("ended", function(){
		audio[26].play();
	});
	
	audio[26].addEventListener("ended", function(){
		girlSpeakAnimated2.fadeOut(0);
	});
	
	audio[26].addEventListener("ended", function(){
		examplePic.css("background-image", "");
		mainCloud.fadeOut(500);
		boySpeakAnimated3.fadeIn(0);
		audio[27].play();
	});
	
	audio[27].addEventListener("ended", function(){
		boySpeakAnimated3.fadeOut(0);
		timeout[8] = setTimeout(function(){
			audio[28].play();
			girlSpeakAnimated2.fadeIn(0);
			wideCloud.fadeIn(0);
			examplePic.css("background-image", "url('pics/metals-plus-acid.gif')");
		}, 3000);
	});
	
	audio[28].addEventListener("ended", function(){
		audio[29].play();
	});
	
	audio[29].addEventListener("ended", function(){
		audio[30].play();
	});
	
	audio[30].addEventListener("ended", function(){
		audio[31].play();
		examplePic.css("background-image", "url('pics/inactive-metals-plus-acid.gif')");
	});
	
	audio[31].addEventListener("ended", function(){
		audio[32].play();
	});
	
	audio[32].addEventListener("ended", function(){
		examplePic.css("background-image", "");
		wideCloud.fadeOut(0);
		girlSpeakAnimated2.fadeOut(0);
		frame4Eyes.fadeOut(0);
		frameSprite.to(5, true);
		timeout[8] = setTimeout(function(){
			audio[33].play();
			boySpeakAnimated4.fadeIn(0);
			boySpeakAnimated4Eyes.fadeIn(0);
			examplePic.css("background-image", "url(pics/vocabulary-cloud.png)");
		}, 2000);
	});
	
	audio[33].addEventListener("ended", function(){
		boySpeakAnimated4.fadeOut(0);
		timeout[7] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 5000);
	});
		
	var startButtonListener = function(){
		blackSkin.fadeOut(500);
		startButton.fadeOut(500);
		timeout[0] = setTimeout(function(){
			audio[0].play();
			girlSpeakAnimated.fadeIn(0);
			frame1Eyes.fadeIn(0);
		}, 2000);
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch102 = function()
{
	var answers = $("#frame-102 .answer"),
		keepThinking = $("#frame-102 .keep-thinking, #frame-102 .black-skin"),
		activeTask = 1, 
		tasks = $(".task");
		
	var audio = new Audio("audio/applause.mp3");
	
	keepThinking.fadeOut(0);
	
	tasks.fadeOut(0); 
	$(".task-"+activeTask).fadeIn(0);
	empty = $("#frame-102 .task-" + activeTask + " .empty");
	
	var answersListener = function(){
		var tempImage = $(this).css("background-image");
		empty.css("background-image", tempImage);
		if($(this).attr("data-key") === empty.attr("data-key"))
		{
			empty.css("background-color", "#01A6E8");
			audio.play();
			timeout[0] = setTimeout(function(){
				activeTask++;
				tasks.fadeOut(0); 
				$(".task-"+activeTask).fadeIn(0);
				empty = $("#frame-102 .task-" + activeTask + " .empty");
				if(activeTask > 3)
					hideEverythingBut($("#frame-103"));
			}, 2000);
		}
		else
		{
			empty.css("background-color", "#FF3C25");
			keepThinking.fadeIn(0);
			timeout[0] = setTimeout(function(){
				keepThinking.fadeOut(0);
			}, 2000);
		}
	};
	
	answers.off("click", answersListener);
	answers.on("click", answersListener);
}

var launch103 = function()
{
	var blackSkin = $("#frame-103 .black-skin"), 
		startButton = $("#frame-103 .start-button"), 
		nextButton = $("#frame-103 .next-button");
		wideCloud = $("#frame-103 .wide-cloud"), 
		videoContainer = $("#frame-103 .video-container"),
		video1 = $("#frame-103 #reaction"),
		boySpeakAnimated = $("#frame-103 .boy-speak-animated"), 
		girlSpeakAnimated = $("#frame-103 .girl-speak-animated"),
		boyGirlEyes = $("#frame-103 .boy-girl-eyes"),
		carbonates = $("#frame-103 .carbonates");
	
	audio[0] = new Audio("audio/boy-7.mp3"),
	audio[1] = new Audio("audio/girl-30.mp3"),
	audio[2] = new Audio("audio/girl-31.mp3");
	audio[3] = new Audio("audio/girl-32.mp3");
	audio[4] = new Audio("audio/girl-33.mp3");
	audio[5] = new Audio("audio/girl-34.mp3");
	audio[6] = new Audio("audio/girl-35.mp3");
	
	carbonatesSprite = new Motio(carbonates[0], {
		fps: 2, 
		frames: 12
	});
	
	carbonates.fadeOut(0);
		
	boySpeakAnimated.fadeOut(0);
	girlSpeakAnimated.fadeOut(0);
	wideCloud.fadeOut(0);
	videoContainer.fadeOut(0);
	nextButton.fadeOut(0);
	
	video1.attr("width", videoContainer.css("width"));
	video1.attr("height", videoContainer.css("height"));
	
	carbonatesSprite.on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	audio[0].addEventListener("ended", function(){
		boySpeakAnimated.fadeOut(0);
		girlSpeakAnimated.fadeIn(0);
		audio[1].play();
		wideCloud.fadeIn(500);
		timeout[0] = setTimeout(function(){
			carbonates.fadeIn(0);
			carbonatesSprite.play();
		}, 1000);
	});
	
	audio[1].addEventListener("ended", function(){
		wideCloud.fadeOut(0);
		carbonates.fadeOut(0);
		audio[2].play();	
	});
	
	audio[2].addEventListener("ended", function(){
		audio[3].play();	
		blackSkin.fadeIn(0);
		videoContainer.fadeIn(0);
		video1.play();
		alert(vide1.muted);
	});
	
	audio[3].addEventListener("ended", function(){
		audio[4].play();	
	});
	
	audio[4].addEventListener("ended", function(){
		audio[5].play();	
	});
	
	audio[5].addEventListener("ended", function(){
		audio[6].play();	
	});
	
	audio[6].addEventListener("ended", function(){
		videoContainer.fadeOut(0);
		nextButton.fadeIn(0);
		girlSpeakAnimated.fadeOut(0);
	});
	
	var startButtonListener = function(){
		blackSkin.fadeOut(500);
		startButton.fadeOut(500);
		timeout[0] = setTimeout(function(){
			audio[0].play();
			boySpeakAnimated.fadeIn(0);
			frame1Eyes.fadeIn(0);
		}, 2000);
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch104 = function()
{
	var draggables = $("#frame-104 .ball");
	var draggabillies = [],
		vegetable, basket,
		blackSkin = $("#frame-104 .black-skin"),
		nextButton = $("#frame-104 .next-button");
		
	blackSkin.fadeOut(0);
	nextButton.fadeOut(0);
	
	for (var i = 0; i < draggables.length; i ++)
		draggabillies[i] = new Draggabilly(draggables[i]);
	
	var onStart = function(instance, event, pointer)
	{
		vegetable = $(event.target);
	};
	var onEnd= function(instance, event, pointer)
	{
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(vegetable.attr("data-key") === basket.attr("data-key"))
		{
			basket.html(vegetable.html());
			vegetable.remove();
			if(!$("#frame-104 .ball").length)
			{
				blackSkin.fadeIn(500);
				nextButton.fadeIn(1000);
			}
		}	
		else
		{
			vegetable.fadeIn(0);
			vegetable.css("left", "");
			vegetable.css("top", "");
		}	
	};
	
	for (var i = 0; i < draggabillies.length; i ++)
	{
		draggabillies[i].on("dragStart", onStart);
		draggabillies[i].on("dragEnd", onEnd);
	}
}

var launch106 = function()
{
	var allMouthes = $("#all-mouthes");
	var audio = new Audio("audio/surprise.mp3");
	
	allMouthes.fadeOut(0);
	
	audio.addEventListener("ended", function(){
		allMouthes.fadeOut(0);
	});
	
	timeout[0] = setTimeout(function(){
		allMouthes.fadeIn(0);
		audio.play();
	}, 1000);
}

var launch301 = function()
{
	var nextButton = $("#frame-301 .next-button"),
		parts = $("#frame-301 .part"),
		answers = $(".task .answer"), 
		result = $(".result .label"), 
		correctNum = 0;
	
	resultLabel = result.html();
	
	parts.fadeOut(0);
	$("part-1").fadeIn(0);
	
	answers.on("click", function(){
		if($(this).attr("data-correct"))
		{
			correctNum ++;
			result.html(resultLabel + " " + correctNum);
		}
	});
}

var hideEverythingBut = function(elem)
{
	frames.fadeOut(0);
	elem.fadeIn(0);
	
	switch(elem.attr("id"))
	{
		case "frame-101": 
			launch101();
			break;
		case "frame-102":
			launch102();
			break;
		case "frame-103":
			launch103();
			break;
		case "frame-104":
			launch104();
			break;
		case "frame-106":
			launch106();
			break;
		case "frame-301":
			launch301();
			break;
	}
}

var initOtherData = function()
{
	
}

var initAudios = function()
{
	buttonSound = new Audio("audio/button-sound.mp3");
}

var initJquerySelectors = function()
{
	derButton = $("#der-button");
	factsButton = $("#facts-button");
	labButton = $("#lab-button");
	links = $(".link");
	menuFrame = $("#frame-000");
	frames = $(".frame");
	model = $("#model");
	modelContent = $("#model #model-content");
}

var initMenuButtons = function(){
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
		buttonSound.play();
	});
};

var showModel = function(content){
	model.fadeIn(1000);
	modelContent.html(content);
};

var hideModel = function(){
	model.fadeOut(1000);
};

var showMap = function()
{
	
}
var main = function()
{
	initAudios();
	initJquerySelectors();
	initMenuButtons();
	hideEverythingBut(menuFrame);
	//hideEverythingBut($("#frame-201"));
};

$(document).ready(main);